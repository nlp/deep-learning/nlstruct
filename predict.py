#!/bin/env python3

from nlstruct import load_pretrained
from nlstruct.datasets import load_from_brat, export_to_brat
import pandas as pd
from nlstruct.base import *
from nlstruct.datasets import *
from carbontracker.tracker import CarbonTracker
import argparse
import os


def make_dir(d):
    try:
        os.makedirs(d)
    except FileExistsError as error:
        print(d+" already exists")


BASE_WORD_REGEX = r'(?:[\w]+(?:[’\'])?)|[!"#$%&\'’\(\)*+,-./:;<=>?@\[\]^_`{|}~]'

if "display" not in globals():
    display = print

#test_data = "/vol/usersiles2/bannour/NER_Merlot/QUAERO_FrenchMed_BRAT/corpus/test/EMEA/"
#outpath = "/vol/usersiles2/bannour/NER_Merlot/nlstruct-MoreExp/out/LREC/QuaeroFrenchMed/EMEA/RUN-mBERT/ResultsTestEMEA"
#ner_model = load_pretrained("/vol/usersiles2/bannour/NER_Merlot/nlstruct-MoreExp/checkpoints/mBERT/my_xp_EMEA-mBERT-1b01d40b06a57eac-00839.ckpt")

parser = argparse.ArgumentParser(description = 'Predict NLStruct toolkit')
parser.add_argument('-m','--model', metavar = 'model', type = str, help = 'model checkpoint file (.ckpt)', required = True)
parser.add_argument('-o','--output', metavar = 'output', type = str, help = 'output directory', required = True)
parser.add_argument('-d','--data', metavar = 'data', type = str, help = 'Test data directory', required = True)
args = parser.parse_args()

outpath = args.output
test_data = args.data
ner_model = load_pretrained(args.model)

make_dir(args.output+"/test/")
make_dir(args.output+"/train/")
make_dir(args.output+"/dev/")



dataset = BRATDataset(
        train=None,
        val=None,
        test=test_data
)

#metrics = {
#        "exact": dict(module="dem", binarize_tag_threshold=1., binarize_label_threshold=1., word_regex=BASE_WORD_REGEX),
#        "half_word": dict(module="dem", binarize_tag_threshold=0.5, binarize_label_threshold=1., word_regex=BASE_WORD_REGEX),
#        "any_word": dict(module="dem", binarize_tag_threshold=1e-5, binarize_label_threshold=1., word_regex=BASE_WORD_REGEX),
#}

tracker = CarbonTracker(epochs=1, verbose=2, monitor_epochs=-1)
tracker.epoch_start()


test_predictions = ner_model.predict(dataset.test_data)
tracker.epoch_end()
print('test', test_predictions)
#print(ww)
#display(type(test_predictions), test_predictions)
#test_results = ner_model.metrics(test_predictions, dataset.test_data)
if outpath is not None:
   export_to_brat(test_predictions, filename_prefix=outpath)

#display(pd.DataFrame(test_results).T)
