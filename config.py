from os.path import join

BASE_WORD_REGEX = r'(?:[\w]+(?:[’\'])?)|[!"#$%&\'’\(\)*+,-./:;<=>?@\[\]^_`{|}~]'
BASE_SENTENCE_REGEX = r"((?:\s*\n)+\s*|(?:(?<=[a-z0-9)]\.)\s+))(?=[A-Z-])"

model_list = {
        "fralbert":{'model':'qwant/fralbert-base', 'lower':True},
        "mbert":{'model':'bert-base-multilingual-uncased', 'lower':True},
        "bertlarge":{'model':'bert-large-uncased', 'lower':True},
        "flaubert":{'model':'flaubert/flaubert_base_uncased', 'lower':True},
        "camembert":{'model':'almanach/camembert-base', 'lower':False},
        "camembert-bio":{'model':'almanach/camembert-bio-base', 'lower':False},
        "drbert":{'model':'Dr-BERT/DrBERT-4GB', 'lower':False},
        "xmlroberta":{'model':'FacebookAI/xlm-roberta-base', 'lower':False},
}

data = {
        'emea':
            {'datadir':'datasets/EMEA', 
            'train_dir' : '/train/',
            'val_dir' : '/dev/',
            'test_dir' : '/test/',
            },
        'deft':
            {'datadir':'datasets/DEFT/', 
            'train_dir' : '/train/',
            'val_dir': '/dev/',
            'test_dir' : '/test/',
            },
        'e3c':
            {'datadir':'datasets/e3c/',
            'train_dir' : '/training/',
            'val_dir': '/test/',
            'test_dir' : '/test/',
            },
        'medline':
            {'datadir':'datasets/MEDLINE/',
            'train_dir' : '/train/',
            'val_dir': '/dev/',
            'test_dir' : '/test/',
            },
}
id_config = 1

class Config:
    def __init__(self, model_name='mbert', task='emea'):
        # Dataset name   
        self.dataset_name = task+'-'+str(model_name) # any name, just for logging
        # where to store produced data (not output, but for example fasttext embeddings)
        self.datadir = 'data/'      
        # output directory (for labeled test data)
        self.output_dir = 'out/'+task+'/RUN-'+str(model_name) ## For output files
        self.xp_name = str(model_name)+ '/' + 'my_xp_' + self.dataset_name 
        
        #######
        ## DATA
        #######
        # Train, val, test directories,
        # containing Brat datasets
        datadir = data[task]['datadir']
        self.train_dir = datadir + data[task]['train_dir']
        self.val_dir   = datadir + data[task]['val_dir']
        self.test_dir = datadir + data[task]['test_dir']
#        self.train_dir = '/home/c.servan/Projects/data/QUAERO_FrenchMed_BRAT/corpus/train/EMEA_debug/'
#        self.val_dir   = '/home/c.servan/Projects/data/QUAERO_FrenchMed_BRAT/corpus/dev/EMEA_debug/'
#        self.test_dir = '/home/c.servan/Projects/data/QUAERO_FrenchMed_BRAT/corpus/test/EMEA_debug/'
        
        
        # Does the data contains nested entities?
        # Edit `nested` to fit your data
        # True: no constraint (entities can overlap, nest with no restriction)
        # False: no entity overlaping at all
        # no_same_label: entities can be nested, but not if the have the same label
        #self.nested = "no_same_label"
        self.nested = True
        #assert self.nested in [True, False, "no_same_label"]
        assert self.nested in [True, False, True]
        
        #######
        ## MODELS
        #######
        ## BERT
        #self.bert_model = 'camembert/camembert-base'
        #self.bert_model = 'almanach/camembert-bio-base'
        #self.bert_model = 'Dr-BERT/DrBERT-4GB'
        #self.bert_model = 'bert-large-uncased'
#        self.bert_model = 'qwant/fralbert-base'
        #self.bert_model = 'flaubert/flaubert_base_uncased'
        self.bert_model = model_list[model_name]["model"]
        
        ## FASTTEXT
        self.fasttext_embeddings = join(self.datadir, f'{self.dataset_name}_fasttext.txt')
        self.fasttext_lang = 'fr'               # en for English
        self.fasttext_model = 'cc.fr.300.bin'   # cc.en.300.bin for English data, 
                                                # or your own model if you have one
            
        self.max_epochs = 20
        self.finetune_bert = True
        self.lower = model_list[model_name]["lower"]
        
        #######
        ## MISC
        #######
        self.word_regex = BASE_WORD_REGEX
        self.sentence_regex = BASE_SENTENCE_REGEX
        
        # Seeds
        # The training will run for each seed, so there will be
        # len(seeds) training.
        # Choose only one seed when developping, but 
        # give more to obtain robust results to evaluate your system
        #self.seeds = (42, )  # the training will run for each seed
        self.seeds = (42, 1558, 555, 123, 456, 789)   # the training will run for each seed

    def set_epoch(self, e):
        self.max_epochs = e
