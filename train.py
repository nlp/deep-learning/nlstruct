from os.path import isfile, isdir, join, dirname
from os import makedirs
import logging
import operator
import fasttext
import fasttext.util
import argparse

from nlstruct.datasets.brat import load_from_brat
from nlstruct.data_utils import regex_tokenize

from nlstruct.recipes import train_ner

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    datefmt="%m/%d/%Y %H:%M:%S",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = 'Train NLStruct toolkit')
    parser.add_argument('-m','--model', metavar = 'model', type = str, help = 'model name (list in config.py)', required = True)
    parser.add_argument('-t','--task', metavar = 'task', type = str, help = 'NER task (list in config.py)', required = True)
    parser.add_argument('-e','--epoch', metavar = 'epoch', type = int, help = 'Number of epochs to be trained, override the config file', required = False)
    parser.add_argument('-d','--date', metavar = 'date', type = str, help = 'Current date to set up the checkpoint directory', required = False)
    parser.add_argument('-s','--seed', metavar = 'seed', type = int, help = 'Current seed to specify, default in config.py', required = False)
    args = parser.parse_args()
    # Load config 
    logger.info(f'   Load configuration')
    from config import Config

    conf = Config(model_name=args.model, task=args.task)
    if args.epoch != None:
        conf.max_epochs = args.epoch
    print("Max epoch to train:", conf.max_epochs)
    if args.date != None:
        conf.xp_name = conf.xp_name + "_" + args.date
        conf.output_dir = conf.output_dir + "_" + args.date

    print("Output dir to train:", conf.output_dir)
    if args.seed != None:
        conf.seeds = [args.seed]
    print("Seed to train:", conf.seeds)
#    print("date to train:", args.date)


    # Sanity checks
    error = False
    if not isdir(conf.train_dir):
        logger.error(f'Train dir {conf.train_dir} does not exist')
        error = True
    """if conf.val_dir is not None:
        if not isdir(conf.val_dir):
            logger.error(f'Validation dir {conf.val_dir} does not exist')
            error = True"""
    if conf.test_dir is not None:
        if not isdir(conf.test_dir):
            logger.error(f'Test dir {conf.test_dir} does not exist')
            error = True
    if conf.nested not in [True, False, "no_same_label"]:
        logger.error(f'Unknown value {conf.nested} for configuration parameter `nested`')
        error = True
    try:
        ft_dir = dirname(conf.fasttext_embeddings)
        if not isdir(ft_dir):
            makedirs(ft_dir)
    except Exception as err:
        logging.error(f'Could not create fasttext directory {conf.fasttext_embeddings}')
        raise err

    if error:
        raise Exception('Sanity check has failed, read the error messages')

    word_split_regex = conf.word_regex

    ##########
    ## Create FASTTEXT embeddings
    ##########
    if isfile(conf.fasttext_embeddings):
        logger.info(
            f'   Fasttext embeddings already created ({conf.fasttext_embeddings}). Remove this file to compute them from scratch')
    else:
        logger.info(
            f"   Let's build fasttext embeddings for this dataset (will be saved to {conf.fasttext_embeddings})")
        logger.info(f'   Load fasttext {conf.fasttext_model} ({conf.fasttext_lang})')
        fasttext.util.download_model(conf.fasttext_lang, if_exists='ignore')
        ft = fasttext.load_model(conf.fasttext_model)
        # Load corpora
        # Note that we also load test dataset here for simplicity.
        #   It could be done at evaluation time with the exact same result.
        datasets = [load_from_brat(conf.train_dir)]
        if conf.val_dir is not None:
            datasets.append(load_from_brat(conf.val_dir))
        if conf.test_dir is not None:
            datasets.append(load_from_brat(conf.test_dir))

        # Word-tokenize corpora
        logger.info('   Tokenize datasets')

        # Collect words and rank them by frequency
        all_words = {}
        for dataset in datasets:
            for doc in dataset:
                words = regex_tokenize(doc['text'], reg=word_split_regex)
                for word in words['text']:
                    n = all_words.get(word, 0)
                    all_words[word] = n + 1

        logger.info(f'   Build fasttext embeddings for the {len(all_words)} found words')
        with open(conf.fasttext_embeddings, 'w') as ft_out:
            for word, _ in sorted(all_words.items(), key=operator.itemgetter(1), reverse=True):
                emb = ft.get_word_vector(word)
                ft_out.write(word + ' ' + ' '.join(['{:.6f}'.format(e) for e in emb]) + '\n')
        logger.info(f'   fasttext embeddings written to {conf.fasttext_embeddings}')

    ##########
    ## Train pyner
    ##########
    if len(conf.seeds) > 1:
        logger.info(f'Will run the training {len(conf.seeds)} times...')
    for i, seed in enumerate(conf.seeds):
        logger.info(f'{i + 1}. Train {conf.dataset_name} with seed {seed} (nested={conf.nested})')
        if conf.output_dir:
            out_brat_dir = join(conf.output_dir, f'{i + 1}')
            result_file = join(conf.output_dir, 'results.csv')
            if not isdir(out_brat_dir):
                makedirs(out_brat_dir)
        else:
            out_brat_dir = None

        model = train_ner(
            dataset={
                "train": conf.train_dir,
                "val": conf.val_dir,
                "test": conf.test_dir,
        },
        finetune_bert = conf.finetune_bert,
            seed = seed,
            bert_name = conf.bert_model,
            fasttext_file = conf.fasttext_embeddings,
            gpus = 1,
            xp_name = conf.xp_name,
            bert_lower = conf.lower,
            max_epochs = conf.max_epochs,
        )
        #model.save_pretrained("ner_camembBIO.pt")

        if out_brat_dir is not None:
            logger.info(f'Brat results written to {out_brat_dir}')
        with open(result_file, 'a') as f_out:
            f_out.write(f'{conf.dataset_name}\t{seed}\t{model}\n')
        logger.info(f'   Train pyner DONE for {conf.dataset_name}/{i}, results written to {model}')
    logger.info(f'DONE')


