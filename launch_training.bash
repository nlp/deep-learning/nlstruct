mdate=`date '+%Y-%m-%d_%H-%M-%S'`
seed=555 
gpu=0
Help()
{
	echo "Chain launch script:"
	echo ""
	echo "Syntax: chain_launch.bash [-g|h]"
	echo "options:"
	echo "	-g: gpu ID"
        echo "  -e: epoch"
	echo "	-m: models"
	echo "	-t: tasks"
	echo "	-h: this help"
	echo ""
	echo "list of models available:"
	echo "fralbert mbert flaubert camembert camembert-bio drbert"
	echo "list of tasks available:"
	echo "emea e3c deft medline"
}
while getopts ":hg:m:t:e:" option; do
   case $option in
       h) # display Help
            Help
            exit;;
       g) # gpu
            gpu=$OPTARG;;
       e) # epoch
	    epoch=$OPTARG;;	    
       m) #models
	    models=$OPTARG;;
       t) #taks
	    tasks=$OPTARG;;
       \?) # Invalid option
            echo "Error: Invalid option"
            exit;;
   esac
done

echo "GPU to use: $gpu"
echo "models to use: $models"
echo "tasks to perform: $tasks"
for seed in 42 1558 555 123 456 789
do
	for model in $models
	do
		for task in $tasks
		do
			echo "*********************** Launching $model with $task using seed $seed ***********************"
			mdate=`date '+%Y-%m-%d_%H-%M-%S'`
			time CUDA_VISIBLE_DEVICES=$gpu python train.py -m $model -t $task -e $epoch -d $mdate -s $seed >& log.$model.$task.$epoch.$mdate.txt
		done 
	done
done
